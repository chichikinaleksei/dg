<?php
/**
 * datagen functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package datagen
 */

// Enqueue scripts & styles
add_action( 'wp_enqueue_scripts', 'ch_scripts' );
function ch_scripts() {
  wp_enqueue_style( 'ch-styles', get_template_directory_uri() . '/css/main.css' );
  wp_enqueue_script( 'ch-libs', get_template_directory_uri() . '/js/libs.min.js', [], '1.0.0', true );
  wp_enqueue_script( 'ch-scripts', get_template_directory_uri() . '/js/main.js', [], '1.0.0', true );
}
// Put styles into admin
add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );
function load_custom_wp_admin_style() {
  wp_register_style( 'custom_wp_admin_css', get_template_directory_uri() . '/css/main.css', false, '1.0.0' );
  wp_enqueue_style( 'custom_wp_admin_css' );
}

// Includes
require get_template_directory() . '/inc/acf-functions/data-gen.php';

// Image size
add_theme_support( 'post_thumbnails', array( 'top', 'bottom' ) );
add_image_size( 'common-top', 1360, 458, true );
add_image_size( 'common-bottom', 387, 509, true );


// Remove embed & wp blocks
add_action( 'wp_footer', 'my_deregister_scripts' );
function my_deregister_scripts(){
  wp_deregister_script( 'wp-embed' );
}
add_action( 'wp_print_styles', 'wps_deregister_styles', 100 );
function wps_deregister_styles() {
  wp_dequeue_style( 'wp-block-library' );
}
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );



// Custom categories for Guttenberg blocks

function my_blocks_plugin_block_categories( $categories ) {
  return array_merge(
    $categories,
    array(
      array(
        'slug' => 'datagen-blocks',
        'title' => __( 'DataGen Blocks', 'datagen-blocks' ),
        'icon'  => 'wordpress',
      ),
    )
  );
}
add_filter( 'block_categories', 'my_blocks_plugin_block_categories', 1, 2 );


// Remove embed js
function disable_embeds_code_init() {
  // Remove the REST API endpoint.
  remove_action( 'rest_api_init', 'wp_oembed_register_route' );
  // Turn off oEmbed auto discovery.
  add_filter( 'embed_oembed_discover', '__return_false' );
  // Don't filter oEmbed results.
  remove_filter( 'oembed_dataparse', 'wp_filter_oembed_result', 10 );
  // Remove oEmbed discovery links.
  remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
  // Remove oEmbed-specific JavaScript from the front-end and back-end.
  remove_action( 'wp_head', 'wp_oembed_add_host_js' );
  add_filter( 'tiny_mce_plugins', 'disable_embeds_tiny_mce_plugin' );
  // Remove all embeds rewrite rules.
  add_filter( 'rewrite_rules_array', 'disable_embeds_rewrites' );
  // Remove filter of the oEmbed result before any HTTP requests are made.
  remove_filter( 'pre_oembed_result', 'wp_filter_pre_oembed_result', 10 );
}
add_action( 'init', 'disable_embeds_code_init', 9999 );

function disable_embeds_tiny_mce_plugin($plugins) {
  return array_diff($plugins, array('wpembed'));
}

function disable_embeds_rewrites($rules) {
  foreach($rules as $rule => $rewrite) {
    if(false !== strpos($rewrite, 'embed=true')) {
      unset($rules[$rule]);
    }
  }
  return $rules;
}