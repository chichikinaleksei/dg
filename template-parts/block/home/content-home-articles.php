<?php
/**
 * Block Name: Home articles
 */
if(get_field('open_modal_with_see_all_articles_button')) {
  $opsaa = 'open-modal';
} else {
  $opsaa = '';
}
?>

<div class="articles">
  <div class="container container--wide">
    <div class="section-header flex row jcsb">
      <h2 class="ch-title ch-title--middle ch-title--bold"><?php echo get_field('title'); ?></h2>
      <a class="link link--smaller link--desktop <?php echo $opsaa; ?>" href="<?php echo get_field('see_all_articles'); ?>" target="_blank">
        <div class="link__wrapper link__wrapper--smaller">
          <span>See All Articles</span>
          <span>See All Articles</span>
        </div>
        <div class="link__img link__img--small">
          <img src="<?php echo get_stylesheet_directory_uri() ?>/img/right-arrow-white.svg" alt="<?php echo get_field('title'); ?>">
        </div>
      </a>
    </div>
  </div>
  <div class="articles__wrapper" id="articles">

    <?php if( have_rows('articles') ): ?>
      <?php while( have_rows('articles') ): the_row(); ?>
        <div class="article">
          <div class="article__wrapper flex row aic">
            <div class="article__img" style="background-image: url('<?php echo get_sub_field('image'); ?>')">
            </div>
            <div class="article__info">
              <p class="ch-text ch-text--big ch-text--dark"><?php echo get_sub_field('title'); ?></p>
              <div class="article__info-wrapper flex row jcsb aife">
                <p class="article__by ch-text ch-text--dark">
                  By <span><?php echo get_sub_field('author'); ?></span>
                </p>
                <a class="article__btn" href="<?php echo get_sub_field('article_link'); ?>" target="_blank">
                  <div class="article__btn-arrow">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/img/right-arrow-white.svg" alt="<?php echo get_sub_field('title'); ?>">
                  </div>
                  <div class="article__btn-arrow article__btn-arrow--blue">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/img/right-arrow-blue.svg" alt="<?php echo get_sub_field('title'); ?>">
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div>
      <?php endwhile; ?>
    <?php endif; ?>

  </div>
  <a class="link link--smaller link--mobile" href="<?php echo get_field('see_all_articles'); ?>" target="_blank">
    <div class="link__wrapper link__wrapper--smaller">
      <span>See All Articles</span>
      <span>See All Articles</span>
    </div>
    <div class="link__img link__img--small">
      <img src="<?php echo get_stylesheet_directory_uri() ?>/img/right-arrow-white.svg" alt="<?php echo get_field('title'); ?>">
    </div>
  </a>
  <div class="circle circle--8" data-rellax-speed="0"></div>
</div>


</section>