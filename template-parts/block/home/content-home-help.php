<?php
/**
 * Block Name: Home help
 */

?>

<div class="help">
  <div class="container">
    <h2 class="ch-title ch-title--middle ch-title--dark-blue ch-title--600 text-center"><?php echo get_field('title'); ?></h2>
  </div>
  <div class="help__wrapper flex row aic">

    <?php if( have_rows('links') ): ?>
      <?php while( have_rows('links') ): the_row(); ?>
        <a class="help__btn flex row aic jcsb open-modal" href="">
          <?php echo get_sub_field('link_title') ?>
          <div class="help__btn-img">
            <img class="help__btn-arrow-white" src="<?php echo get_stylesheet_directory_uri() ?>/img/right-arrow-white.svg">
            <img class="help__btn-arrow-blue" src="<?php echo get_stylesheet_directory_uri() ?>/img/right-arrow-blue.svg">
          </div>
        </a>
      <?php endwhile; ?>
    <?php endif; ?>

  </div>

</div>
