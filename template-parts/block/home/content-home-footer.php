<?php
/**
 * Block Name: Home footer
 */

?>

<div class="footer">
  <div class="footer__wrapper flex row jcsb">
    <div class="footer__inner-wrapper">
      <a class="logo" href="/">
        <img src="<?php echo get_field('logo'); ?>" alt="Data For Understanding The World">
      </a>
    </div>
    <div class="footer__inner-wrapper flex row jcsb">
      <div class="footer__column">
        <h6 class="footer__title">Visit</h6>
        <?php if( have_rows('addresses') ): ?>
          <?php while( have_rows('addresses') ): the_row(); ?>
            <div class="footer__link"><?php echo get_sub_field('address'); ?></div>
          <?php endwhile; ?>
        <?php endif; ?>
      </div>
<!--      <div class="footer__column">-->
<!--        <h6 class="footer__title">Call</h6>-->
<!--        --><?php //if( have_rows('phones') ): ?>
<!--          --><?php //while( have_rows('phones') ): the_row(); ?>
<!--            <a class="footer__link" href="tel:--><?php //echo get_sub_field('phone'); ?><!--">--><?php //echo get_sub_field('phone'); ?><!--</a>-->
<!--          --><?php //endwhile; ?>
<!--        --><?php //endif; ?>
<!--      </div>-->
      <div class="footer__column">
        <h6 class="footer__title">Email</h6>
        <?php if( have_rows('mails') ): ?>
          <?php while( have_rows('mails') ): the_row(); ?>
            <a class="footer__link" href="mailto:info@datagen,tech"><?php echo get_sub_field('mail'); ?></a>
          <?php endwhile; ?>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>
</section>

